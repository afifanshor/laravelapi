<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Repositories\RateRepositoryInterface;
use App\Services\Billing;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BillingTest extends TestCase
{
    use DatabaseMigrations;

    function testMotorLane1Feature()
    {
        // Arrange
        $rates = [
            ['vehicle_class_id' => 1, 'price' => 10000, 'lane' => 1],
            ['vehicle_class_id' => 2, 'price' => 25000, 'lane' => 1],
            ['vehicle_class_id' => 3, 'price' => 30000, 'lane' => 1],
            ['vehicle_class_id' => 4, 'price' => 50000, 'lane' => 1],
        ];

        $rateRepo = app(RateRepositoryInterface::class);
        foreach ($rates as $rate) {
            $rateRepo->create($rate);
        }

        $billing                = new Billing($rateRepo);
        $passage                = new \stdClass();
        $passage->vehicle_class = 1;
        $passage->lane = 1;

        // Act
        $rate = $billing->rate($passage);
        dd($rateRepo->all());
        // Assert
        $this->assertDatabaseHas('rates',$rates);
        //$this->assertEquals(10000, $rate);
    }
}