<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\Calculator;


class CalculatorTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    // Test For Add
    public function testAdd()
    {
        $calculator = new Calculator();
        $x = 1;
        $y = 2;
        $result = $calculator->add($x, $y);
        $this->assertEquals($x + $y, $result);
    }
    public function testAddWithString()
    {
        $calculator = new Calculator();
        $x = "Tiga";
        $y = "4";
        $result = $calculator->add($x, $y);
        $this->assertEquals(false, $result);
    }

    // Test for Divide
    public function testDiv()
    {
        $calculator = new Calculator();
        $x = 3;
        $y = 4;
        $result = $calculator->div($x, $y);
        $this->assertEquals($x / $y, $result);
    }
    public function testDivWithString()
    {
        $calculator = new Calculator();
        $x = "Tiga";
        $y = "Empat";
        $result = $calculator->div($x, $y);
        $this->assertEquals(false, $result);
    }
}
