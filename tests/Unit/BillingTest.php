<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Services\Billing;
use App\Repositories\RateRepositoryInterface;
use Mockery as m;

class BillingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    function testMotorLane1()
    {
        // Arrange
        $rates = [
            (object)['vehicle_class_id' => 1, 'price' => 10000,'lane'=>1 ],
            (object)['vehicle_class_id' => 2, 'price' => 25000,'lane'=>1 ],
            (object)['vehicle_class_id' => 3, 'price' => 30000,'lane'=>1 ],
            (object)['vehicle_class_id' => 4, 'price' => 50000, 'lane'=>1],
        ];

        $rateRepo = m::mock(RateRepositoryInterface::class);
        $rateRepo->shouldReceive('all')->andReturn($rates);

        $billing = new Billing($rateRepo);
        $passage = new \stdClass();
        $passage->vehicle_class = 1;
        $passage->lane=1;

        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(10000, $rate);
    }
    function testMotorLane2()
    {
        // Arrange
        $rates = [
            (object)['vehicle_class_id' => 1, 'price' => 10000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 2, 'price' => 25000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 1, 'price' => 20000, 'lane' => 2, ],
            (object)['vehicle_class_id' => 2, 'price' => 30000, 'lane' => 2, ],
        ];

        $rateRepo = m::mock(RateRepositoryInterface::class);
        $rateRepo->shouldReceive('all')->andReturn($rates);

        $billing = new Billing($rateRepo);
        $passage = new \stdClass();
        $passage->vehicle_class = 1;
        $passage->lane = 2;
        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(20000, $rate);
    }
    function testMobilLane1()
    {
        // Arrange
        $rates = [
            (object)['vehicle_class_id' => 1, 'price' => 10000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 2, 'price' => 25000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 1, 'price' => 20000, 'lane' => 2, ],
            (object)['vehicle_class_id' => 2, 'price' => 30000, 'lane' => 2, ],
        ];

        $rateRepo = m::mock(RateRepositoryInterface::class);
        $rateRepo->shouldReceive('all')->andReturn($rates);

        $billing = new Billing($rateRepo);
        $passage = new \stdClass();
        $passage->vehicle_class = 2;
        $passage->lane = 2;
        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(30000, $rate);
    }
    function testMobilLane2()
    {
        // Arrange
        $rates = [
            (object)['vehicle_class_id' => 1, 'price' => 10000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 2, 'price' => 25000, 'lane' => 1, ],
            (object)['vehicle_class_id' => 1, 'price' => 20000, 'lane' => 2, ],
            (object)['vehicle_class_id' => 2, 'price' => 30000, 'lane' => 2, ],
        ];

        $rateRepo = m::mock(RateRepositoryInterface::class);
        $rateRepo->shouldReceive('all')->andReturn($rates);

        $billing = new Billing($rateRepo);
        $passage = new \stdClass();
        $passage->vehicle_class = 2;
        $passage->lane = 2;
        // Act
        $rate = $billing->rate($passage);

        // Assert
        $this->assertEquals(30000, $rate);
    }
}
