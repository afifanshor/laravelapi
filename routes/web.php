<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/passage/receive', 'PassageController@receive');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/get/api','ApiController@getModifiedAbsensi');
Route::any('/get/oriAPIToday','ApiController@getAPIAbsensyThisMonth');
Route::get('/get/token','ApiController@getToken');
Route::post('/post/apifromWSO2','ApiController@getAbsensiFromWSO2');
Route::get('/get/apifromWSO2','ApiController@getAbsensiFromWSO2');
Route::get('/get/apifromWSO2ForNana','ApiController@getAbsensiFromWSO2ForNana');
	