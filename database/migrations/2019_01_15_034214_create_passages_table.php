<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePassagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->unsigned()->nullable();
            $table->integer('vehicle_class_id')->unsigned()->nullable();
            $table->string('license_plate')->nullable();
            $table->integer('lane')->nullable();
            $table->integer('rate_id')->unsigned()->nullable();
            $table->integer('price')->nullable();
            $table->integer('type_id')->nullable();
            $table->integer('status_id')->nullable();
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('vehicle_class_id')->references('id')->on('vehicle_classes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passages');
    }
}
