<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enforcement extends Model
{
    protected $fillable = ['passage_id', 'type_id', 'fine', 'status_id'];
}
