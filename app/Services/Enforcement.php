<?php

namespace App\Services;

use DB;
/**
 * Enforcement service
 *
 */
class Enforcement
{
    /**
     * Check and save if enforced
     * 
     * @param Passage $passage
     * @param Account $account
     * @return boolean
     */
    function submit($passage, $account = null)
    {
        if ($account == null) {
            DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 2, 'fine' => 100000, 'status_id' => 1]);
        } else {
            if ($account->balance < $passage->price) {
                DB::table('enforcements')->insert(['passage_id' => $passage->id, 'type_id' => 1, 'fine' => 50000, 'status_id' => 1]);
            } else {
                return false;
            }
        }
        return false;
    }

}