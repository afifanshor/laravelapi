<?php

namespace App\Services;

use App\Repositories\RateRepositoryInterface;

/**
 * Billing service
 *
 */
class Billing
{
    protected $rateRepo;
    protected $rates;

    function __construct(RateRepositoryInterface $rateRepo)
    {
        $this->rateRepo = $rateRepo;
        $this->rates    = $this->rateRepo->all();
    }

    /**
     * Hitung vehicle class menjadi rupiah
     *
     * @param Passage $passage
     */
    function rate($passage)
    {
        foreach ($this->rates as $rate) {
            if (((int) $passage->vehicle_class === (int) $rate->vehicle_class_id) && ((int) $passage->lane === (int) $rate->lane)) {
                return $rate->price; // return first found
                //return 0;
            }
        }
        return null;
    }
}