<?php

namespace App\Services;

class Calculator
{
	function add($a, $b)
	{
		if((!is_numeric($a))||(!is_numeric($b)))
			return false;
		return $a+$b;
	}
	function div($a,$b){
		if((!is_numeric($a))||(!is_numeric($b)))
			return false;
		return $a/$b;
	}
}