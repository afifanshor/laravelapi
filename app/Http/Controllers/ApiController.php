<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class ApiController extends Controller{
	public static $appTokenProduction = 'THluTE9pY0ZtV2ZLMEdKeWVUQ3pCTjVFc1NnYTpiU2t3bjlTbFV4bG5WVHhzNUdkTDN5ekRsVHNh';
	public static $appTokenSandbox = 'cFpuekZtSk1BRVRSSXF2ZlR5UnJnOTZ0MjkwYTp4ckg0T1E3OUR4M19qOHlVWHAyd04zX3g2TXNh';

	//function sample for get api absensi with return json by jaka
	//input 	: request 
	//output 	: json absensi from portal absen swamedia 
	public function getapi(Request $request){
		$unit 	= $request->get('unit');
		$tanggal = $request->get('tanggal');
		$url = "http://portal.swamedia.co.id/index.php/hrm/json/".$unit."/".$tanggal ;    
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_HTTPGET, 1);

		$json_response = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($json_response, true);

       	return json_encode($response);
	}

	//function sample for get api absensi with param unit and tanggal
	//input 	: unit = string, tanggal = date 
	//output 	: json absensi from portal absen swamedia 
	public function getAPIAbsensiByParam($unit, $tanggal){
		$url = "http://portal.swamedia.co.id/index.php/hrm/json/".$unit."/".$tanggal ;    
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
		curl_setopt($curl, CURLOPT_HTTPGET, 1);

		$json_response = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($json_response, true);

       	return json_encode($response);
	}
		
	//function sample for get api absensi today with param unit 
	//input 	: unit = string, tanggal = date 
	//output 	: json absensi from portal absen swamedia 
	public function getAPIAbsensyThisMonth($unit='9'){
		$tanggal = date('mY');
		return $this->getAPIAbsensiByParam($unit,$tanggal);
	}

	//function modified get absensi from portal absen
	//input 	: 	request 
	//output 	: 	modified with these return value name, waktuDatang, waktuPulang, keterangan 
	//				from original json absensi from portal absen swamedia 
	public function getModifiedAbsensi(Request $request){
		//echo($request->unit);
		$response = json_decode($this->getAPIAbsensyThisMonth('9'), true);
		$object_array = array();
		if(!$response)
			return json_encode(['error' => 'could not connect API portal absensi']);
		foreach ($response['absen'] as $absen){
			if (end($absen['rincian'])['tanggal']==date("d-M-Y")){
                if ((end($absen['rincian'])['status']=='absen')||(end($absen['rincian'])['status']=='spj')){
                    $object = (object) [
						'nama' => $absen['nama'],
						'foto' =>str_replace("user_icon","",$absen['foto']),
                        'waktuDatang' => end($absen['rincian'])['masuk'],
                        'waktuPulang' => end($absen['rincian'])['pulang'],
                        'keterangan' => end($absen['rincian'])['keterangan'],
                    ];
                    array_push($object_array,$object);
                }
			}
			// buka bila perlu memberi keterangan pada yang belum absen
			// else
			// 	$object = (object) [
			// 		'nama' => $absen['nama'],
			// 		'waktuAbsen' => '-',
			// 		'keterangan' => 'Belum Absen',
			// 	];
		}
		usort($object_array,array($this,'comparator'));
       	return json_encode($object_array);
	}
		
	//function get token for wso2
	//input 	: 	request 
	//output 	: 	token from wso2 from app with production token defined above
	public function getToken(Request $request=null ){
		$ch = curl_init();
		$url_token_wso2	= "http://172.17.3.13:8280/token";

		curl_setopt($ch, CURLOPT_URL, $url_token_wso2);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		$headers = array();
		$headers[] = 'Authorization: Basic '.self::$appTokenProduction ;
		$headers[] = 'Content-Type: application/x-www-form-urlencoded';
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		
		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);die;
		}
		return json_decode($result)->access_token;
	}
		
	//function get json format from wso2 
	//input 	: 	request 
	//output 	: 	json from wso2 
	public function getAbsensiFromWSO2ForNana(Request $request ){
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'http://172.17.3.13:8280/AbsensiPerorang/1.0.0/get/'.$request->unit);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


		$headers = array();
		$headers[] = 'Accept: application/json';

		// $headers[] = 'Authorization: Bearer '.'c4577458-68ba-3946-8520-1dc53363cb13';
		$headers[] = 'Authorization: Bearer '.$this->getToken();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		 $object = (object) ['data'=>json_decode($result)];
		return (json_encode($object));
	}

	//function get json format from wso2 
	//input 	: 	request 
	//output 	: 	json from wso2 
	public function getAbsensiFromWSO2(Request $request ){
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'http://172.17.3.13:8280/AbsensiPerorang/1.0.0/get/'.$request->unit);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);


		$headers = array();
		$headers[] = 'Accept: application/json';

		// $headers[] = 'Authorization: Bearer '.'c4577458-68ba-3946-8520-1dc53363cb13';
		$headers[] = 'Authorization: Bearer '.$this->getToken();
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);
		if (curl_errno($ch)) {
			echo 'Error:' . curl_error($ch);
		}
		return ($result);
	}
		
	//function comparator for sorting method
	//input 	: 	object 1, object 2 
	//output 	: 	bool status if the object 1 is higher than object 2
	public function comparator($object1, $object2) { 
		return strtotime($object1->waktuDatang) > strtotime($object2->waktuDatang); 
	} 
}