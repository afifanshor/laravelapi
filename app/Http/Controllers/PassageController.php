<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Account;
use App\Passage;
use App\VehicleClass;
use App\Services\Billing;
use App\Repositories\AccountRepositoryInterface;
use App\Repositories\EnforcementRepositoryInterface;
use App\Repositories\PassageRepositoryInterface;
use App\Repositories\VehicleClassRepositoryInterface;

class PassageController extends Controller
{
 function __construct(
    AccountRepositoryInterface $accountRepo, EnforcementRepositoryInterface $enforcementRepo,
    PassageRepositoryInterface $passageRepo, VehicleClassRepositoryInterface $vehicleClassRepo)
 {
    $this->accountRepo      = $accountRepo;
    $this->enforcementRepo  = $enforcementRepo;
    $this->passageRepo      = $passageRepo;
    $this->vehicleClassRepo = $vehicleClassRepo;
}

function receive(Request $request)
    {
        $passage = new Passage();

        $account = $this->accountRepo->getActiveByNumber($request->account_number);

        $vehicleClass   = $this->vehicleClassRepo->getByCode($request->vehicle_class);
        $vehicleClassId = ($vehicleClass != '') ? $vehicleClass->id : null;

        $billing = app(Billing::class);
        $harga   = $billing->rate($request);

        $passage->price            = $harga;
        $passage->vehicle_class_id = $vehicleClassId;
        $passage->license_plate    = $request->license_plate_number;
        $passage->lane             = $request->lane;
        $passage->account_id       = $account != '' ? $account->id : null;
        $passage->save();

        $enforcement = new Enforcement();
        $enforcement->submit($passage, $account);

        if ($account != '') {
            $account->balance = $account->balance - $harga;
            $account->save();
        }

        echo 'success';
    }
}