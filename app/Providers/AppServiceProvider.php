<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \App\Repositories\AccountRepositoryInterface::class, \App\Repositories\AccountRepository::class
        );
        $this->app->bind(
            \App\Repositories\EnforcementRepositoryInterface::class, \App\Repositories\EnforcementRepository::class
        );
        $this->app->bind(
            \App\Repositories\PassageRepositoryInterface::class, \App\Repositories\PassageRepository::class
        );
        $this->app->bind(
            \App\Repositories\RateRepositoryInterface::class, \App\Repositories\RateRepository::class
        );
        $this->app->bind(
            \App\Repositories\VehicleClassRepositoryInterface::class, \App\Repositories\VehicleClassRepository::class
        );
        $this->app->bind(
            \App\Repositories\AccountRepositoryInterface::class, \App\Repositories\AccountRepository::class
        );
    }
}