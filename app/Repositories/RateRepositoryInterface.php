<?php

namespace App\Repositories;

interface RateRepositoryInterface
{

    function all();

    function create($rates);
}