<?php

namespace App\Repositories;

use App\Repositories\AccountRepositoryInterface;
use App\Account;

class AccountRepository implements AccountRepositoryInterface
{

    function getActiveByNumber($number)
    {
        return Account::where('number', $number)->where('status_id', self::STATUS_ACTIVE)->first();
    }
}